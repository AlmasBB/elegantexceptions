/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almas.exceptions.test;

import com.almas.exceptions.catches.Catch;
import com.almas.exceptions.processes.VoidProc;
import com.almas.exceptions.tries.CheckedTry;
import com.almas.exceptions.tries.UnCheckedTry;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author almas337519@gmail.com
 */
public final class TryTest extends Assert {

    private static final Logger LOG = Logger.getLogger(TryTest.class.getName());

    @Test
    public void test() throws Exception {
        new UnCheckedTry(
                new CheckedTry(
                        new Catch(new Consumer<Exception>() {
                            @Override
                                public void accept(Exception t) {
                                    LOG.log(Level.SEVERE, t.getMessage());
                                }
                        }, IOException.class))
                    .withFinally(new VoidProc() {
                            @Override
                            public void exec() throws Exception {
                                System.out.println("TUT");
                    }
                })).exec(new VoidProc() {
            @Override
            public void exec() throws Exception {
                System.out.println("HELLO");
            }
        });
    }

}
