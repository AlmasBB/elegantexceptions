/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almas.exceptions.test;

import com.almas.exceptions.catches.Catch;
import com.almas.exceptions.tries.CheckedTry;
import com.almas.exceptions.tries.UnCheckedTry;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author almas337519@gmail.com
 */
public final class UncheckedTryTest extends Assert{
    
    
    @Test(expected = RuntimeException.class)
    public void checkedThrowException(){
        new UnCheckedTry(
                new CheckedTry(
                        new Catch(
                                (exc)->System.out.println("Exception is throws"),
                                IllegalArgumentException.class
                        )
                )
        ).exec(UncheckedTryTest::throwException);
    }
    
    private static void throwException(){
        if(true){
            throw new IllegalArgumentException();
        }
    }
    
}
