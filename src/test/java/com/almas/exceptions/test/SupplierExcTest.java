/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almas.exceptions.test;

import com.almas.exceptions.catches.Catch;
import com.almas.exceptions.tries.CheckedTry;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author almas337519@gmail.com
 */
public final class SupplierExcTest extends Assert{
    
    @Test
    public void isLegalSupplier() throws Exception{
        assertTrue(new CheckedTry(
                        new Catch(
                            (exc)->System.out.println("Never happened"),
                            IllegalStateException.class
                        )
                   ).exec(()->100)==100);
    }
    
}
