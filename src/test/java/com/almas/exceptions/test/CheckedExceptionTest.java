/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almas.exceptions.test;

import com.almas.exceptions.catches.Catch;
import com.almas.exceptions.tries.CheckedTry;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author almas337519@gmail.com
 */
public final class CheckedExceptionTest extends Assert {

    @Test(expected = IllegalArgumentException.class)
    public void executeCheckedException() throws Exception {
        new CheckedTry(
                new Catch(
                        (exception) -> System.out.println("Wrong argument"),
                        IllegalArgumentException.class)
        ).exec(CheckedExceptionTest::throwException);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testCatches() throws Exception {
        final AtomicInteger number = new AtomicInteger(0);
        try {
            new CheckedTry(
                    new Catch(
                            (exception) -> number.incrementAndGet(),
                            IllegalArgumentException.class
                    ),
                    new Catch(
                            (exception) -> number.incrementAndGet(),
                            IllegalArgumentException.class
                    ),
                    new Catch((exception) -> number.incrementAndGet(), NullPointerException.class)
            ).exec(CheckedExceptionTest::throwException);
        } finally {
            assertTrue(number.get() == 2);
        }
        
    }

    private static void throwException() {
        if(true){
            throw new IllegalArgumentException();
        }
       
    }
   
}
