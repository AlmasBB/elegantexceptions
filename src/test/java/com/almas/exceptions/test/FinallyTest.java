/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.almas.exceptions.test;

import com.almas.exceptions.catches.Catch;
import com.almas.exceptions.tries.CheckedTry;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author almas337519@gmail.com
 */
public final class FinallyTest extends Assert {

    @Test(expected = IllegalArgumentException.class)
    public void checkedFinallyWithExceptionThrowing() throws Exception {
        final AtomicInteger number = new AtomicInteger(0);
        try {
            new CheckedTry(
                    new Catch(
                            (exception) -> number.getAndIncrement(),
                            IllegalArgumentException.class
                    )
            ).withFinally(() -> number.getAndIncrement())
             .exec(FinallyTest::throwException);
        } finally {
            assertTrue(number.get() == 2);
        }
    }
    
    
    @Test
    public void checkedFinallyBlock() throws Exception {
        final AtomicInteger number = new AtomicInteger(0);
        new CheckedTry(
                new Catch(
                        (exc) -> System.out.print("Never happened"),
                        IllegalArgumentException.class
                )
        ).withFinally(()->number.getAndIncrement())
         .exec(()->System.out.print("Never happened"));
        assertTrue(number.get()==1);
    }

    private static void throwException() {
        if (true) {
            throw new IllegalArgumentException();
        }
    }

}
