package com.almas.exceptions.catches;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author almas337519@gmail.com
 */
public final class Catch implements Catchable {
    /**
     * List of exceptions to be catched
     */
    private final List<Class<? extends Exception>> exceptions;
    
    /**
     * Action to provide in catch block
     */
    private final Consumer<Exception> action;

    public Catch(final List<Class<? extends Exception>> exceptions,
            final Consumer<Exception> consumer) {
        this.exceptions = Collections.unmodifiableList(exceptions);
        this.action = consumer;
    }
    
    
    public Catch(final Consumer<Exception> consumer,final Class<? extends Exception> ... exceptions){
        this(Arrays.asList(exceptions),consumer);
    }
    
    /**
     * 
     * @param exc to be handled
     */
    @Override
    public void handle(final Exception exc) {
        if (this.support(exc)) {
            this.action.accept(exc);
        }
    }
    
    /**
     * 
     * @param exc to check
     * @return <p>true if original list contain this exception</p>
     */
    @Override
    public boolean support(final Exception exc) {
        return this.exceptions.stream()
                   .anyMatch(_class -> _class.isInstance(exc));
    }

}
