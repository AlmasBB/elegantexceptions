package com.almas.exceptions.catches;

/**
 *
 * @author almas337519@gmail.com
 */
public interface Catchable {
    
    void handle(Exception exc);
    
    boolean support(Exception exc);
}
