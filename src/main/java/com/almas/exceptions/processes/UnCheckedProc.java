package com.almas.exceptions.processes;

/**
 *This class wrap original VoidProc and wrap exception into RuntimeException
 * 
 * @author almas337519@gmail.com
 */
public final class UnCheckedProc implements VoidProc {

    private final VoidProc origin;

    public UnCheckedProc(final VoidProc proc) {
        this.origin = proc;
    }

    @Override
    public void exec() {
        try {
            this.origin.exec();
        } catch (final Exception exc) {
            throw new RuntimeException(exc);
        }
    }

}
