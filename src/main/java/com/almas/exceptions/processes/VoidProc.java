package com.almas.exceptions.processes;

/**
 *
 * @author almas337519@gmail.com
 */
public interface VoidProc {
    
    void exec() throws Exception;
    
}
