package com.almas.exceptions.tries;

import com.almas.exceptions.catches.Catchable;
import com.almas.exceptions.processes.VoidProc;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

/**
 *This class provide an API to handle exception using the List of Catchables
 * 
 * 
 * 
 * @author almas337519@gmail.com
 */
public final class CheckedTry implements Checkable<Exception> {

    private final List<Catchable> catchables;

    public CheckedTry(final List<Catchable> catchables) {
        this.catchables = Collections.unmodifiableList(catchables);
    }

    public CheckedTry(final Catchable... catchables) {
        this(Arrays.asList(catchables));
    }
    
    
    /**
     * 
     * @param prc to be executed
     * @throws Exception if fail
     */
    @Override
    public void exec(final VoidProc prc) throws Exception {
        try {
            prc.exec();
        } catch (final Exception exc) {
            catchables.forEach(catchable -> catchable.handle(exc));
            throw exc;
        }
    }
    
    
    /**
     * 
     * @param <T> return type
     * @param splr that will return value on success
     * @return value from supplier
     * @throws Exception if fails
     */
    @Override
    public <T> T exec(final Supplier<T> splr) throws Exception {
        try {
            return splr.get();
        } catch (final Exception exc) {
            catchables.forEach(catchable -> catchable.handle(exc));
            throw exc;
        }
    }
    
    /**
     * 
     * @param fnly process to be executed in finally block
     * @return TryWithFinally that wrap original class
     */
    public Checkable<Exception> withFinally(final VoidProc fnly) {
        return new TryWithFinally(this, fnly);
    }

 
}
