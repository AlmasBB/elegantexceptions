package com.almas.exceptions.tries;

import com.almas.exceptions.processes.VoidProc;
import java.util.function.Supplier;

/**
 *This class wrap exception from origin Checkable into RuntimeException
 * 
 * 
 * @author almas337519@gmail.com
 */
public final class UnCheckedTry implements Checkable<Exception> {

    private final Checkable<Exception> origin;

    public UnCheckedTry(final Checkable<Exception> checkable) {
        this.origin = checkable;
    }

    @Override
    public void exec(final VoidProc prc) {
        try {
            this.origin.exec(prc);
        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public <T> T exec(final Supplier<T> splr) {
        try {
            return splr.get();
        } catch (final Exception exc) {
            throw new RuntimeException(exc);
        }
    }

}
