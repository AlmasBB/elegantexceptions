package com.almas.exceptions.tries;

import com.almas.exceptions.processes.VoidProc;
import java.util.function.Supplier;

/**
 *This class incapsulate original Checkable 
 * and add finally block to exec method of origin
 * 
 * @author almas337519@gmail.com
 */
public final class TryWithFinally implements Checkable<Exception>{
    
    private final Checkable<Exception> origin;
    
    private final VoidProc proc;

    public TryWithFinally(final Checkable<Exception> checkable,final VoidProc proc) {
        this.origin = checkable;
        this.proc = proc;
    }
    
    
    
    @Override
    public void exec(final VoidProc prc) throws Exception {
        try{
            this.origin.exec(prc);
        } finally{
            proc.exec();
        }
    }

    @Override
    public <T> T exec(final Supplier<T> splr) throws Exception {
        try{
            return this.origin.exec(splr);
        }finally{
            this.proc.exec();
        }
    }
    
}
