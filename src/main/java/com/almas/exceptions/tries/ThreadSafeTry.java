package com.almas.exceptions.tries;

import com.almas.exceptions.processes.VoidProc;
import java.util.function.Supplier;

/**
 * This class incapsulate original Chekable and guarantees the thread safety
 *
 * @author almas337519@gmail.com
 */
public final class ThreadSafeTry implements Checkable<Exception> {

    private final Checkable<Exception> origin;

    public ThreadSafeTry(final Checkable<Exception> origin) {
        this.origin = origin;
    }

    @Override
    public void exec(final VoidProc prc) throws Exception {
        synchronized (this) {
            this.origin.exec(prc);
        }
    }

    @Override
    public <T> T exec(final Supplier<T> splr) throws Exception {
        synchronized (this) {
           return this.origin.exec(splr);
        }
    }

}
