package com.almas.exceptions.tries;

import com.almas.exceptions.processes.VoidProc;
import java.util.function.Supplier;

/**
 *
 * @author almas337519@gmail.com
 * @param <E> type of exception
 */
public interface Checkable<E extends Exception> {

    void exec(VoidProc prc) throws Exception;

    <T> T exec(Supplier<T> splr) throws Exception;
}
